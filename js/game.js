const imgArray = [
	"media/img/cat1.jpg",
	"media/img/cat2.jpg",
	"media/img/cat3.jpg",
	"media/img/cat4.jpg",
	"media/img/cat5.jpg",
	"media/img/cat6.jpg",
	"media/img/cat7.jpg",
	"media/img/cat8.jpg",
	"media/img/cat9.jpg"
];

let a = imgArray.length; 
let score = 0;

function showArrayImg() {

	for (let i = 0; i < imgArray.length;  i++) {
		
        const img = new Image();
        img.src = imgArray[i];
        img.style.width = '33%';
        img.style.height = 'auto';
        
        document.getElementById('images').appendChild(img);

	}

}

showArrayImg();

function showRandImg() {

	rand = Math.floor((Math.random() * a));

    const img = new Image();
    img.src = imgArray[rand];
    img.style.width = '50%';

    document.getElementById('random-img').appendChild(img);

}

showRandImg();

function checkImage() {

	var g = document.getElementById('images');
	var randImg = document.getElementById('random-img');

	for (var i = 0, len = g.children.length; i < len; i++) {
	    
	    (function(index){
	        g.children[i].onclick = function(){

	        	if (index == rand) {
    				score++;
	        		document.getElementById('answer').innerHTML = "YEY! Goed geraden! Nog eentje? score:" + score; 

    				randImg.removeChild(randImg.childNodes[0]);

					showRandImg();

	        	} else {
	        		score = 0;
	        		document.getElementById('answer').innerHTML = "Oei! Dat was niet goed. Probeer opnieuw. score: " + score;
	        	}

	        }    
	    })(i);
	    
	}


}

checkImage();
					